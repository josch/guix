#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

# Set guix architecture
GUIX_ARCH = $(DEB_HOST_GNU_CPU)-$(DEB_HOST_ARCH_OS)
ifeq ($(GUIX_ARCH),x86_64-linux)
# The guix bootstrap binaries for amd64 really are i686-linux.
GUIX_ARCH = i686-linux
endif
ifeq ($(GUIX_ARCH),arm-linux)
GUIX_ARCH = armhf-linux
endif

ifeq (,$(filter pkg.guix.parallel,$(DEB_BUILD_PROFILES)))
# Disable parallelism to avoid reproducibility issues:
# https://issues.guix.gnu.org/20272
# https://bugzilla.opensuse.org/show_bug.cgi?id=1170378
PARALLEL = --no-parallel
endif

%:
	dh $@ $(PARALLEL)

override_dh_autoreconf:
	dh_autoreconf ./bootstrap --

override_dh_auto_configure:
	dh_auto_configure  -- --libexecdir=/usr/lib/$(DEB_HOST_MULTIARCH)/ --with-bash-completion-dir=/usr/share/bash-completion/completions/ GZIP=/bin/gzip BZIP2=/bin/bzip2 GUILE_EFFECTIVE_VERSION=3.0

override_dh_auto_test:
	# Copy system binaries for tests
	mkdir -p gnu/packages/bootstrap/$(GUIX_ARCH)
	cp /bin/bash /bin/mkdir /bin/tar /usr/bin/xz gnu/packages/bootstrap/$(GUIX_ARCH)/
	# Disable name resolution with RES_OPTIONS to prevent tests from
	# accessing the network
	# Use UTF-8 locale to work around https://issues.guix.gnu.org/46038
	LC_ALL=C.UTF-8 LANG=C.UTF-8 dh_auto_test --no-parallel -- RES_OPTIONS=attempts:0 AM_SCM_LOG_DRIVER_FLAGS="--brief=no"
	rm -vrf gnu/packages/bootstrap/

override_dh_strip:
	dh_strip --exclude=.go

override_dh_dwz:
	dh_dwz --exclude=.go

override_dh_shlibdeps:
	dh_shlibdeps --exclude=.go

override_dh_install:
	dh_install
	mkdir -p debian/guix/lib/systemd/system/
	mv -vf debian/guix/usr/lib/*/systemd/system/guix-daemon.service \
		debian/guix/lib/systemd/system/guix-daemon.service
	mv -vf debian/guix/usr/lib/*/systemd/system/guix-publish.service \
		debian/guix/lib/systemd/system/guix-publish.service
	mv -vf debian/guix/usr/lib/*/systemd/system/gnu-store.mount \
		debian/guix/lib/systemd/system/gnu-store.mount
	# guix-gc is installed via examples
	rm -f debian/guix/usr/lib/*/systemd/system/guix-gc.*
	# Add /etc/default/acl with the default substitute servers,
	# with identical output as "guix archive --authorize"
	mkdir -p debian/guix/etc/guix/
	printf '(acl\n (entry\n' > debian/guix/etc/guix/acl
	sed -e 's,^,  ,g' -e 's, $$,,g' etc/substitutes/ci.guix.gnu.org.pub >> debian/guix/etc/guix/acl
	printf '  (tag\n   (guix import)\n   )\n  )\n (entry\n' >> debian/guix/etc/guix/acl
	sed -e 's,^,  ,g' -e 's, $$,,g' etc/substitutes/bordeaux.guix.gnu.org.pub >> debian/guix/etc/guix/acl
	printf '  (tag\n   (guix import)\n   )\n  )\n )\n' >> debian/guix/etc/guix/acl
	rmdir debian/guix/usr/lib/*/systemd/system/
	rmdir debian/guix/usr/lib/*/systemd/

override_dh_auto_clean:
	dh_auto_clean
	# Remove build cruft
	rm -vf guix/build/po.go
	# Remove documentation cruft
	rm -vf doc/guix-cookbook.*.info
	rm -vf doc/guix-daemon.1
	rm -vf doc/guix.*.info*
	rm -vf doc/stamp-*
	rm -vf doc/version-*.texi
