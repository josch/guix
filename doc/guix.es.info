This is guix.es.info, produced by makeinfo version 6.7 from
guix.es.texi.

Copyright © 2012-2022 Ludovic Courtès
Copyright © 2013, 2014, 2016 Andreas Enge
Copyright © 2013 Nikita Karetnikov
Copyright © 2014, 2015, 2016 Alex Kost
Copyright © 2015, 2016 Mathieu Lirzin
Copyright © 2014 Pierre-Antoine Rault
Copyright © 2015 Taylan Ulrich Bayırlı/Kammer
Copyright © 2015, 2016, 2017, 2019, 2020, 2021 Leo Famulari
Copyright © 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022 Ricardo
Wurmus
Copyright © 2016 Ben Woodcroft
Copyright © 2016, 2017, 2018, 2021 Chris Marusich
Copyright © 2016, 2017, 2018, 2019, 2020, 2021, 2022 Efraim Flashner
Copyright © 2016 John Darrington
Copyright © 2016, 2017 Nikita Gillmann
Copyright © 2016, 2017, 2018, 2019, 2020 Jan Nieuwenhuizen
Copyright © 2016, 2017, 2018, 2019, 2020, 2021 Julien Lepiller
Copyright © 2016 Alex ter Weele
Copyright © 2016, 2017, 2018, 2019, 2020, 2021 Christopher Baines
Copyright © 2017, 2018, 2019 Clément Lassieur
Copyright © 2017, 2018, 2020, 2021, 2022 Mathieu Othacehe
Copyright © 2017 Federico Beffa
Copyright © 2017, 2018 Carlo Zancanaro
Copyright © 2017 Thomas Danckaert
Copyright © 2017 humanitiesNerd
Copyright © 2017, 2021 Christine Lemmer-Webber
Copyright © 2017, 2018, 2019, 2020, 2021, 2022 Marius Bakke
Copyright © 2017, 2019, 2020, 2022 Hartmut Goebel
Copyright © 2017, 2019, 2020, 2021, 2022 Maxim Cournoyer
Copyright © 2017–2022 Tobias Geerinckx-Rice
Copyright © 2017 George Clemmer
Copyright © 2017 Andy Wingo
Copyright © 2017, 2018, 2019, 2020 Arun Isaac
Copyright © 2017 nee
Copyright © 2018 Rutger Helling
Copyright © 2018, 2021 Oleg Pykhalov
Copyright © 2018 Mike Gerwitz
Copyright © 2018 Pierre-Antoine Rouby
Copyright © 2018, 2019 Gábor Boskovits
Copyright © 2018, 2019, 2020, 2022 Florian Pelz
Copyright © 2018 Laura Lazzati
Copyright © 2018 Alex Vong
Copyright © 2019 Josh Holland
Copyright © 2019, 2020 Diego Nicola Barbato
Copyright © 2019 Ivan Petkov
Copyright © 2019 Jakob L. Kreuze
Copyright © 2019 Kyle Andrews
Copyright © 2019 Alex Griffin
Copyright © 2019, 2020, 2021, 2022 Guillaume Le Vaillant
Copyright © 2020 Liliana Marie Prikler
Copyright © 2019, 2020, 2021, 2022 Simon Tournier
Copyright © 2020 Wiktor Żelazny
Copyright © 2020 Damien Cassou
Copyright © 2020 Jakub Kądziołka
Copyright © 2020 Jack Hill
Copyright © 2020 Naga Malleswari
Copyright © 2020, 2021 Brice Waegeneire
Copyright © 2020 R Veera Kumar
Copyright © 2020, 2021 Pierre Langlois
Copyright © 2020 pinoaffe
Copyright © 2020 André Batista
Copyright © 2020, 2021 Alexandru-Sergiu Marton
Copyright © 2020 raingloom
Copyright © 2020 Daniel Brooks
Copyright © 2020 John Soo
Copyright © 2020 Jonathan Brielmaier
Copyright © 2020 Edgar Vincent
Copyright © 2021, 2022 Maxime Devos
Copyright © 2021 B. Wilson
Copyright © 2021 Xinglu Chen
Copyright © 2021 Raghav Gururajan
Copyright © 2021 Domagoj Stolfa
Copyright © 2021 Hui Lu
Copyright © 2021 pukkamustard
Copyright © 2021 Alice Brenon
Copyright © 2021, 2022 Josselin Poiret
Copyright © 2021 muradm
Copyright © 2021, 2022 Andrew Tropin
Copyright © 2021 Sarah Morgensen
Copyright © 2022 Remco van ’t Veer
Copyright © 2022 Aleksandr Vityazev
Copyright © 2022 Philip M^{c}Grath
Copyright © 2022 Karl Hallsby
Copyright © 2022 Justin Veilleux
Copyright © 2022 Reily Siegel
Copyright © 2022 Simon Streit
Copyright © 2022 (
Copyright © 2022 John Kehayias

   Se garantiza el permiso de copia, distribución y/o modificación de
este documento bajo los términos de la licencia de documentación libre
de GNU (GNU Free Documentation License), versión 1.3 o cualquier versión
posterior publicada por la Free Software Foundation; sin secciones
invariantes, sin textos de cubierta delantera ni trasera.  Una copia de
la licencia está incluida en la sección titulada “GNU Free Documentation
License”.
INFO-DIR-SECTION Administración del sistema
START-INFO-DIR-ENTRY
* Guix: (guix.es).           Gestión del software instalado y la
                               configuración del sistema.
* guix package: (guix.es)Invocación de guix package.  Instalación, borrado
                                                         y actualización de
                                                         paquetes.
* guix gc: (guix.es)Invocación de guix gc.  Recuperar espacio de disco sin
                                               usar.
* guix pull: (guix.es)Invocación de guix pull.  Actualización de la lista
                                                   disponible de paquetes.
* guix system: (guix.es)Invocación de guix system.  Gestión de la
                                                       configuración del
                                                       sistema operativo.
* guix deploy: (guix.es)Invocación de guix deploy.  Gestión de
                                                       configuraciones de
                                                       sistemas operativos en
                                                       máquinas remotas.
END-INFO-DIR-ENTRY

INFO-DIR-SECTION Desarrollo de software
START-INFO-DIR-ENTRY
* guix shell: (guix)Invoking guix shell.  Creating software environments.
* guix environment: (guix.es)Invocación de guix environment.  Construcción
                                                                 de entornos
                                                                 de desarrollo
                                                                 con Guix.
* guix build: (guix.es)Invocación de guix build.  Construcción de
                                                     paquetes.
* guix pack: (guix.es)Invocación de guix pack.  Creación de empaquetados
                                                   binarios.
END-INFO-DIR-ENTRY


Indirect:
guix.es.info-1: 5851
guix.es.info-2: 315384
guix.es.info-3: 619787
guix.es.info-4: 937564
guix.es.info-5: 1225833
guix.es.info-6: 1545230
guix.es.info-7: 1807419
guix.es.info-8: 2109060

Tag Table:
(Indirect)
Node: Top5851
Node: Introducción23040
Ref: Introducción-Footnote-123970
Ref: Introducción-Footnote-224099
Node: Gestión de software con Guix24460
Node: Distribución GNU27564
Ref: Distribución GNU-Footnote-132622
Node: Instalación32779
Ref: Instalación-Footnote-134713
Node: Instalación binaria34962
Node: Requisitos43078
Ref: Requisitos-Footnote-146511
Node: Ejecución de la batería de pruebas46635
Node: Preparación del daemon50414
Node: Configuración del entorno de construcción51769
Ref: Configuración del entorno de construcción-Footnote-156851
Ref: Configuración del entorno de construcción-Footnote-257205
Node: Configuración de delegación del daemon57407
Ref: Configuración de delegación del daemon-Footnote-167473
Node: Soporte de SELinux67609
Node: Invocación de guix-daemon71847
Ref: daemon-substitute-urls75160
Node: Configuración de la aplicación85936
Ref: locales-and-locpath86412
Ref: Configuración de la aplicación-Footnote-194902
Node: Actualizar Guix95093
Node: Instalación del sistema95777
Node: Limitaciones97277
Node: Consideraciones sobre el hardware98365
Node: Instalación desde memoria USB y DVD100456
Node: Preparación para la instalación103297
Node: Instalación gráfica guiada104658
Node: Instalación manual106490
Node: Distribución de teclado y red y particionado107544
Ref: manual-installation-networking108485
Ref: Distribución de teclado y red y particionado-Footnote-1116164
Ref: Distribución de teclado y red y particionado-Footnote-2116355
Node: Procedimiento de instalación116685
Node: Tras la instalación del sistema120891
Node: Instalación de Guix en una máquina virtual122339
Node: Construcción de la imagen de instalación124292
Node: System Troubleshooting Tips125678
Node: Chrooting into an existing system126456
Node: Empezando129448
Node: Gestión de paquetes137579
Node: Características139331
Node: Invocación de guix package144380
Ref: package-cmd-propagated-inputs148957
Ref: profile-manifest155208
Ref: guix-search161273
Ref: export-manifest167914
Ref: Invocación de guix package-Footnote-1170356
Node: Sustituciones170387
Node: Official Substitute Servers172111
Node: Autorización de servidores de sustituciones173717
Node: Obtención de sustiticiones desde otros servidores176523
Node: Verificación de sustituciones181188
Node: Configuración de la pasarela.183013
Node: Fallos en las sustituciones183631
Node: Sobre la confianza en binarios185324
Node: Paquetes con múltiples salidas187217
Node: Invocación de guix gc189841
Node: Invocación de guix pull199608
Node: Invocación de guix time-machine209498
Ref: Invocación de guix time-machine-Footnote-1212726
Node: Inferiores212815
Node: Invocación de guix describe218805
Node: Invocación de guix archive222930
Node: Canales230961
Node: Especificación de canales adicionales233084
Node: Uso de un canal de Guix personalizado234846
Node: Replicación de Guix235879
Node: Verificación de canales238428
Ref: channel-authentication238618
Node: Channels with Substitutes240235
Node: Creación de un canal241318
Node: Módulos de paquetes en un subdirectorio244806
Node: Declaración de dependencias de canales245356
Node: Especificación de autorizaciones del canal247097
Ref: channel-authorizations247350
Ref: Especificación de autorizaciones del canal-Footnote-1251682
Node: URL primaria252060
Node: Escribir de noticias del canal252974
Ref: Escribir de noticias del canal-Footnote-1256416
Node: Desarrollo256452
Node: Invoking guix shell257586
Ref: shell-development-option262625
Ref: shell-manifest264910
Ref: shell-export-manifest265490
Ref: Invoking guix shell-Footnote-1275396
Ref: Invoking guix shell-Footnote-2275563
Node: Invocación de guix environment275691
Ref: Invocación de guix environment-Footnote-1291645
Ref: Invocación de guix environment-Footnote-2292224
Node: Invocación de guix pack292352
Ref: pack-manifest304564
Ref: pack-symlink-option306184
Ref: Invocación de guix pack-Footnote-1309383
Node: La cadena de herramientas de GCC315384
Node: Invocación de guix git authenticate316705
Node: Interfaz programática319586
Node: Módulos de paquetes322467
Ref: Módulos de paquetes-Footnote-1325168
Ref: Módulos de paquetes-Footnote-2325554
Node: Definición de paquetes325910
Node: Referencia de package335255
Ref: package-propagated-inputs340230
Node: Referencia de origin348995
Node: Definición de variantes de paquetes358004
Node: Writing Manifests368795
Ref: package-development-manifest378224
Node: Sistemas de construcción380455
Ref: emacs-build-system435686
Node: Fases de construcción441926
Ref: phase-validate-runpath444535
Ref: Fases de construcción-Footnote-1452335
Node: Utilidades de construcción452512
Node: Search Paths471759
Node: El almacén479870
Node: Derivaciones486375
Node: La mónada del almacén495800
Ref: La mónada del almacén-Footnote-1508815
Node: Expresiones-G509049
Ref: Expresiones-G-Footnote-1536271
Node: Invocación de guix repl536603
Node: Using Guix Interactively539824
Node: Utilidades544275
Node: Invocación de guix build545960
Node: Opciones comunes de construcción548183
Ref: fallback-option550008
Ref: client-substitute-urls550210
Node: Opciones de transformación de paquetes554904
Node: Opciones de construcción adicionales571398
Ref: build-check580685
Node: Depuración de fallos de construcción583507
Node: Invocación de guix edit586752
Node: Invocación de guix download587986
Node: Invocación de guix hash590640
Node: Invocación de guix import593703
Node: Invocación de guix refresh619787
Node: Invoking guix style634835
Node: Invocación de guix lint640153
Ref: Invocación de guix lint-Footnote-1647682
Node: Invocación de guix size647705
Ref: Invocación de guix size-Footnote-1653352
Node: Invocación de guix graph653576
Node: Invocación de guix publish663829
Node: Invocación de guix challenge676879
Ref: Invocación de guix challenge-Footnote-1684799
Node: Invocación de guix copy684834
Ref: Invocación de guix copy-Footnote-1687090
Node: Invocación de guix container687205
Node: Invocación de guix weather688956
Node: Invocación de guix processes695192
Ref: Invocación de guix processes-Footnote-1698944
Node: Foreign Architectures699095
Node: Cross-Compilation700064
Node: Native Builds701643
Node: Configuración del sistema705009
Node: Uso de la configuración del sistema708078
Ref: auto-login to TTY714822
Node: Referencia de operating-system727666
Ref: Referencia de operating-system-Footnote-1738489
Node: Sistemas de archivos738721
Ref: Sistemas de archivos-Footnote-1750387
Ref: Sistemas de archivos-Footnote-2750674
Node: Sistema de archivos Btrfs750966
Node: Dispositivos traducidos755472
Ref: Dispositivos traducidos-Footnote-1761018
Node: Swap Space761384
Node: Cuentas de usuaria766290
Ref: user-account-password770068
Node: Distribución de teclado773194
Node: Localizaciones779469
Ref: Localizaciones-Footnote-1785599
Node: Servicios785742
Node: Servicios base790183
Ref: syslog-configuration-type811815
Ref: syslog-service812180
Ref: guix-configuration-type812701
Ref: guix-publish-service-type825231
Ref: rngd-service829279
Ref: pam-limits-service829571
Node: Ejecución de tareas programadas839763
Node: Rotación del registro de mensajes846364
Node: Networking Setup854095
Node: Servicios de red870103
Node: Actualizaciones no-atendidas937564
Node: Sistema X Window944889
Ref: gdm945386
Ref: wayland-gdm945934
Ref: lightdm958193
Node: Servicios de impresión969216
Ref: Servicios de impresión-Footnote-11006870
Node: Servicios de escritorio1007051
Node: Servicios de sonido1048116
Node: Servicios de bases de datos1054821
Ref: Servicios de bases de datos-Footnote-11067840
Node: Servicios de correo1067955
Node: Servicios de mensajería1149572
Node: Servicios de telefonía1172896
Node: File-Sharing Services1191536
Node: Servicios de monitorización1225833
Ref: prometheus-node-exporter1231198
Ref: zabbix-agent1236207
Ref: zabbix-front-end1239059
Node: Servicios Kerberos1241196
Node: Servicios LDAP1245910
Node: Servicios Web1263699
Ref: NGINX1271286
Ref: nginx-server-configuration1279474
Ref: cuerpo de nginx-location-configuration1283137
Ref: PHP-FPM1298655
Ref: Servicios Web-Footnote-11315310
Node: Servicios de certificados1315345
Node: Servicios DNS1324957
Ref: Servicios DNS-Footnote-11358704
Ref: Servicios DNS-Footnote-21358736
Node: VNC Services1358769
Node: Servicios VPN1364866
Node: Sistema de archivos en red1378377
Node: Samba Services1385808
Node: Integración continua1391084
Node: Servicios de gestión de energía1401895
Node: Servicios de audio1420938
Node: Servicios de virtualización1425549
Ref: transparent-emulation-qemu1447314
Ref: Servicios de virtualización-Footnote-11490237
Ref: Servicios de virtualización-Footnote-21490272
Node: Servicios de control de versiones1490308
Ref: Servicios de control de versiones-Footnote-11539268
Node: Servicios de juegos1545230
Node: Servicio PAM Mount1546351
Node: Servicios de Guix1550112
Ref: Servicios de Guix-Footnote-11567272
Node: Servicios de Linux1567377
Ref: Servicios de Linux-Footnote-11576622
Node: Servicios de Hurd1576662
Node: Servicios misceláneos1577972
Node: Programas con setuid1603773
Node: Certificados X.5091607036
Node: Selector de servicios de nombres1610129
Node: Disco en RAM inicial1615891
Node: Configuración del gestor de arranque1626445
Node: Invocación de guix system1641932
Ref: guix system vm1652382
Ref: system-extension-graph1668153
Ref: system-shepherd-graph1668799
Ref: Invocación de guix system-Footnote-11669191
Node: Invocación de guix deploy1669353
Node: Ejecutar Guix en una máquina virtual1680087
Node: Definición de servicios1686352
Node: Composición de servicios1687096
Node: Tipos de servicios y servicios1690240
Node: Referencia de servicios1695682
Ref: provenance-service-type1705427
Node: Servicios de Shepherd1708533
Node: Complex Configurations1718199
Node: Home Configuration1731160
Node: Declaring the Home Environment1734082
Node: Configuring the Shell1738427
Node: Home Services1740476
Node: Essential Home Services1742671
Node: Shells Home Services1751168
Ref: home-bash-configuration1753110
Node: Mcron Home Service1759909
Node: Power Management Home Services1761854
Node: Shepherd Home Service1764981
Node: Secure Shell1766658
Node: Desktop Home Services1773596
Node: Guix Home Services1777183
Node: Invoking guix home1778539
Node: Documentación1790401
Ref: Documentación-Footnote-11792400
Node: Platforms1792513
Node: platform Reference1793093
Node: Supported Platforms1794701
Node: System Images1796127
Node: image Reference1797936
Node: partition Reference1807420
Node: Instantiate an Image1809894
Node: image-type Reference1814693
Node: Image Modules1818937
Node: Instalación de archivos de depuración1820609
Node: Información separada para depuración1821826
Node: Reconstrucción de la información para depuración1825373
Node: Using TeX and LaTeX1828365
Node: Actualizaciones de seguridad1833078
Node: Lanzamiento inicial1838064
Node: Lanzamiento inicial a partir de la semilla binaria reducida1840585
Ref: Lanzamiento inicial a partir de la semilla binaria reducida-Footnote-11844761
Ref: Lanzamiento inicial a partir de la semilla binaria reducida-Footnote-21845006
Ref: Lanzamiento inicial a partir de la semilla binaria reducida-Footnote-31845195
Ref: Lanzamiento inicial a partir de la semilla binaria reducida-Footnote-41845293
Node: Preparación para usar los binarios del lanzamiento inicial1845425
Ref: Preparación para usar los binarios del lanzamiento inicial-Footnote-11854990
Node: Transportar1855172
Node: Contribuir1857775
Node: Construcción desde Git1859588
Node: Ejecución de Guix antes de estar instalado1864751
Ref: Ejecución de Guix antes de estar instalado-Footnote-11868439
Node: La configuración perfecta1868653
Node: Pautas de empaquetamiento1873260
Node: Libertad del software1877370
Node: Nombrado de paquetes1878946
Node: Versiones numéricas1880709
Node: Sinopsis y descripciones1884773
Node: snippets frente a fases1888748
Node: Paquetes Emacs1889780
Node: Módulos Python1891989
Node: Módulos Perl1896659
Node: Paquetes Java1897538
Node: Crates de Rust1898478
Node: Paquetes Elm1900691
Node: Tipos de letra1903850
Node: Estilo de codificación1905874
Node: Paradigma de programación1906579
Node: Módulos1906978
Node: Tipos de datos y reconocimiento de patrones1907629
Node: Formato del código1908647
Node: Envío de parches1910558
Ref: Envío de parches-Footnote-11920950
Node: Configuring Git1921873
Node: Envío de una serie de parches1923142
Ref: Single Patches1923372
Ref: Notifying Teams1925718
Ref: Multiple Patches1926202
Node: Teams1928185
Node: Seguimiento de errores y parches1929345
Node: The Issue Tracker1929845
Node: Debbugs User Interfaces1930435
Ref: Debbugs User Interfaces-Footnote-11931638
Node: Debbugs Usertags1931854
Ref: Debbugs Usertags-Footnote-11933887
Node: Acceso al repositorio1934048
Ref: Acceso al repositorio-Footnote-11944940
Node: Actualizar el paquete Guix1945082
Node: Writing Documentation1946719
Node: Traduciendo Guix1948572
Node: Reconocimientos1963591
Node: Licencia de documentación libre GNU1964728
Node: Índice de conceptos1990116
Node: Índice programático2109060

End Tag Table


Local Variables:
coding: utf-8
End:
